package dev.rehm;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SyncApp {

    public static void main(String[] args) {

        /*
            The goal of this example is to see how two threads interact with shared resources. Here,
            we create two threads that will interact with one StringBuilder object and one StringBuffer
            object. As we know, the StringBuffer and the StringBuilder have the same methods; however,
            the StringBuffer's methods are all synchronized, making it a threadsafe implementation. Let's
            take a look at the implications of that.
         */

        ExecutorService multiThreadExecutor = Executors.newFixedThreadPool(2);

        StringBuilder sbuild = new StringBuilder();
        StringBuffer sbuff = new StringBuffer();

        // we provide our task with the StringBuilder and StringBuffer objects, and give this
            // task to our ExecutorService to execute twice with its two threads. There is only one
            // instance of the StringBuilder and StringBuffer that is being referenced, so their
            // tasks will both be completed with the same objects. See the task definition below.
        Runnable stringTask = new StringTestRunnable(sbuff, sbuild);
        multiThreadExecutor.execute(stringTask);
        multiThreadExecutor.execute(stringTask);

        multiThreadExecutor.shutdown();

        try {
            // this is not relevant for the synchronization example, this is a practice
                // done with ExecutorServices to shut it down properly
            if(!multiThreadExecutor.awaitTermination(10, TimeUnit.SECONDS)){
                System.out.println("waiting for shutdown");
            }
        } catch (InterruptedException e) {
            return;
        }

        // Run this method a few times, you should see that the StringBuffer's result are consistent;
            // 100 "~" characters everytime. The StringBuilder results on the other hand are not.
        System.out.println("String Buffer: ");
        System.out.println(sbuff);
        System.out.println("---\nString Builder:");
        System.out.println(sbuild);
    }


    /*
        The StringTestRunnable defines a task for our thread that takes a
        provided instance of a StringBuilder and StringBuffer, and appends 50
        characters (specifically "~" 50 times). When this task is performed
        twice, once by each thread, one would *expect* that there be a contiguous
        100 character string of "~"s.
     */
    static class StringTestRunnable implements Runnable{

        StringBuilder stringBuilder;
        StringBuffer stringBuffer;

        public StringTestRunnable(StringBuffer stringBuffer, StringBuilder stringBuilder){
            this.stringBuffer = stringBuffer;
            this.stringBuilder = stringBuilder;
        }

        @Override
        public void run() {
            for(int i=0; i<50;i++){
                stringBuilder.append("~");
                stringBuffer.append("~");
            }
        }
    }
}
