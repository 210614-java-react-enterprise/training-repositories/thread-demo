package dev.rehm;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorApp {

    public static void main(String[] args) {

        /*
        // we can create standalone threads by creating a thread object
            // and providing a task (or an implementation of a Runnable) to them
        Thread t = new Thread(()-> System.out.println(Thread.currentThread().getName()+" says hello world"));
        t.start();
         */

        /*
        // using an executor uses threads more efficiently, as we can reuse them for our tasks
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        Runnable task1 = ()->{
            System.out.println("task-1 is running in "+Thread.currentThread().getName());
        };

        Runnable task2 = ()->{
            System.out.println("task-2 is running in "+Thread.currentThread().getName());
        };

        singleThreadExecutor.execute(task1);
        singleThreadExecutor.execute(task2);

        // this shutdown process is recommended by Oracle, to make sure that the ExecutorService
            // is shut down, while also trying to make sure any pending tasks are completed.
            // ExecutorServices can continue to listen for more tasks, keeping our JVM running,
            // if not shut down properly.
        singleThreadExecutor.shutdown();
        try {
            if (!singleThreadExecutor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                singleThreadExecutor.shutdownNow();
            }
        } catch (InterruptedException e) {
            singleThreadExecutor.shutdownNow();
        }
         */

        // we don't just need to use one thread, but rather can create a thread pool for the appropriate number
            // of user threads we would like to allocate
        ExecutorService multiThreadExecutor = Executors.newFixedThreadPool(3);

        Runnable task1 = ()->System.out.println("task-1 is running in "+Thread.currentThread().getName());
        Runnable task2 = ()->System.out.println("task-2 is running in "+Thread.currentThread().getName());
        Runnable task3 = ()->System.out.println("task-3 is running in "+Thread.currentThread().getName());
        Runnable task4 = ()->System.out.println("task-4 is running in "+Thread.currentThread().getName());
        Runnable task5 = ()->System.out.println("task-5 is running in "+Thread.currentThread().getName());

        multiThreadExecutor.execute(task1);
        multiThreadExecutor.execute(task2);
        multiThreadExecutor.execute(task3);
        multiThreadExecutor.execute(task4);
        multiThreadExecutor.execute(task5);


        multiThreadExecutor.shutdown();
        try {
            if (!multiThreadExecutor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                multiThreadExecutor.shutdownNow();
            }
        } catch (InterruptedException e) {
            multiThreadExecutor.shutdownNow();
        }
    }
}
