package dev.rehm;

public class ThreadIntroApp {

    public static void main(String[] args) {

        /*
            Run this a few times to see the lifecycle of a thread. Note also that it is indeterminant.
            In some cases, t's state is RUNNABLE longer than others. Also in some case, t is in the
            blocked state, when the resource that it is sharing with t2 (the console) is being occupied.
            This nods to the sometimes unpredictable thread scheduling, that is handled by the JVM and
            out of the developer's control.
         */

        Thread t = new Thread(()-> System.out.println("Hello world from my user created thread"));
        Thread t2 = new MyThread();

        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());

        t.start();
        t2.start();

        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());
        System.out.println("Thread state: "+t.getState());

    }

    static class MyThread extends Thread{
        @Override
        public void run() {
            System.out.println("Hello world from my custom thread class");
        }
    }
}
